package com.yds.utils

import android.content.Context
import java.io.File

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 16:33
 * 页面描述: 清除缓存
 */
@Suppress("unused")
object YDSCleanCacheHelper {

    /*** 作者: YDS 时间: 2023/5/29 16:43 描述: 获取应用程序缓存大小 ***/
    fun getCacheSize(context: Context): String {
        var cacheSize = 0L
        try {
            val cacheDir = context.cacheDir
            val externalCacheDir = context.externalCacheDir
            if (cacheDir != null) {
                cacheSize += getFolderSize(cacheDir)
            }
            if (externalCacheDir != null) {
                cacheSize += getFolderSize(externalCacheDir)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return formatSize(cacheSize)
    }

    /*** 作者: YDS 时间: 2023/5/29 16:43 描述: 删除应用程序缓存 ***/
    fun clearCache(context: Context) {
        try {
            val cacheDir = context.cacheDir
            val externalCacheDir = context.externalCacheDir
            if (cacheDir != null) {
                deleteFolder(cacheDir)
            }
            if (externalCacheDir != null) {
                deleteFolder(externalCacheDir)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 16:43 描述: 获取文件夹大小 ***/
    @Suppress("unused")
    fun getFolderSize(folder: File): Long {
        var size = 0L
        try {
            val fileList = folder.listFiles()
            if (!fileList.isNullOrEmpty()) {
                for (file in fileList) {
                    if (file.isFile) {
                        size += file.length()
                    } else if (file.isDirectory) {
                        size += getFolderSize(file)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return size
    }

    /*** 作者: YDS 时间: 2023/5/29 16:43 描述: 删除文件夹 ***/
    @Suppress("unused")
    fun deleteFolder(folder: File) {
        try {
            val fileList = folder.listFiles()
            if (!fileList.isNullOrEmpty()) {
                for (file in fileList) {
                    if (file.isFile) {
                        file.delete()
                    } else if (file.isDirectory) {
                        deleteFolder(file)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 16:43 描述: 格式化文件大小 ***/
    @Suppress("unused")
    fun formatSize(size: Long): String {
        val kb = size / 1024
        if (kb < 1) {
            return "0KB"
        }
        val mb = kb / 1024
        if (mb < 1) {
            return kb.toString() + "KB"
        }
        val gb = mb / 1024
        if (gb < 1) {
            return mb.toString() + "MB"
        }
        return gb.toString() + "GB"
    }
}