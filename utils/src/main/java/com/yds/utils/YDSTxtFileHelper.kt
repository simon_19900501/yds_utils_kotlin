package com.yds.utils

import android.util.Log
import java.io.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 13:55
 * 页面描述: 文本文件-帮助类
 * tips：<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
 */
@Suppress("unused")
object YDSTxtFileHelper {

    /**
     * 将字符串写入到文本文件中
     *
     * @param content
     * @param filePath   文件存储路径
     * @param fileName   文件存储名称
     */
    fun writeTxtToFile(content: String, filePath: String, fileName: String) {
        // 每次写入时，都换行写
        val contentTemp = content + "\r\n"
        try {
            val file = File(filePath + fileName)
            if (!file.exists()) {
                // 生成文件夹之后，再生成文件
                file.parentFile?.mkdirs()
                file.createNewFile()
            }
            val raf = RandomAccessFile(file, "rwd")
            raf.seek(file.length())
            raf.write(contentTemp.toByteArray())
            raf.close()
        } catch (e: Exception) {
            Log.e("Simon", "writeTxtToFile Exception = ${e.message}")
        }
    }

    /**
     * 读取文本文件的内容
     *
     * @param file
     */
    fun readTxtFromFile(file: File): String {
        var content = ""
        if (!file.isDirectory) {
            if (file.name.endsWith("txt")) {
                try {
                    val inputStream: InputStream = FileInputStream(file)
                    val isr = InputStreamReader(inputStream, "UTF-8")
                    val br = BufferedReader(isr)
                    var line: String
                    // 分行读取
                    while (br.readLine().also { line = it } != null) {
                        content += line + "\n"
                    }
                    // 关闭输入流
                    inputStream.close()
                } catch (e: java.lang.Exception) {
                    Log.d("Simon", "readTxtFromFile Exception = ${e.message}")
                }
            }
        }
        return content
    }
}