package com.yds.utils

import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 15:21
 * 页面描述: 防止同一时间段内多次点击-帮助类
 */
@Suppress("unused")
object YDSRepeatClickRefuseHelper {
    // 点击间隔延迟1500毫秒
    private const val delayTime = 1500

    //是否允许点击
    private var isCanClick = true

    // 上一次点击时间戳
    private var lastClickTime: Long = 0

    private val btnHashCodeMap: MutableMap<Int, Long> = HashMap()

    /*** 作者: YDS 时间: 2023/5/29 15:20 描述: 默认点击事件 ***/
    fun clickButton(listener: OnClickListener) {
        clickButton(0, listener)
    }

    /*** 作者: YDS 时间: 2023/5/29 15:20 描述: 传入点击按钮hash值，初始化lastClickTime ***/
    @Suppress("unused")
    fun clickButton(hashCode: Int, listener: OnClickListener) {
        clickButton(hashCode, delayTime, listener)
    }

    /*** 作者: YDS 时间: 2023/5/29 15:21 描述: 点击按钮事件-自定义时间 ***/
    @Suppress("unused")
    fun clickButton(hashCode: Int, delayTime: Int, listener: OnClickListener) {
        calculateTime(hashCode, delayTime, listener)
    }

    /*** 作者: YDS 时间: 2023/5/29 15:21 描述: 时间计算 ***/
    private fun calculateTime(hashCode: Int, delayTime: Int, listener: OnClickListener) {
        // 获取两次点击事件得时间差
        val aLong = btnHashCodeMap[hashCode]
        if (aLong == null) {
            lastClickTime = 0
        }
        val timeDifference = System.currentTimeMillis() - lastClickTime

        // 如果时间差大于我们设定的点击间隔时间，则允许用户点击
        if (timeDifference > delayTime) {
            isCanClick = true
        } else {
            isCanClick = false
            listener.onClickRefuse()
        }
        if (isCanClick) {
            lastClickTime = System.currentTimeMillis()
            btnHashCodeMap[hashCode] = lastClickTime
            listener.onClickEnabled()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 15:21 描述: 允许点击事件回调 ***/
    interface OnClickListener {
        fun onClickEnabled()
        fun onClickRefuse() {}
    }
}