package com.yds.utils

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.core.content.FileProvider
import java.io.File

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 14:30
 * 页面描述: intent跳转-帮助类
 */
@Suppress("unused")
object YDSIntentHelper {
    /**
     * 使用包名启动app
     *
     * @param packageName 应用包名
     */
    fun Context.launchAppWithPackageName(packageName: String) {
        launchAppWithPackageName(packageName, null)
    }

    /**
     * 使用包名启动app
     *
     * @param packageName 应用包名
     * @param bundle      传递数据
     */
    @Suppress("unused")
    fun Context.launchAppWithPackageName(packageName: String, bundle: Bundle?) {
        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.setPackage(packageName)
        val resolveInfoList: List<ResolveInfo> = packageManager.queryIntentActivities(intent, 0)
        if (resolveInfoList.isNullOrEmpty()) return
        val resolveInfo = resolveInfoList.iterator().next()
        val packageNameTemp = resolveInfo.activityInfo.packageName
        val name = resolveInfo.activityInfo.name
        val intentStart = Intent(Intent.ACTION_MAIN, null)
        bundle?.let { intentStart.putExtras(it) }
        intentStart.addCategory(Intent.CATEGORY_LAUNCHER)
        val componentName = ComponentName(packageNameTemp, name)
        intentStart.component = componentName
        startActivity(intentStart)
    }

    /**
     * 使用Action启动app
     *
     * @param action
     * @param categories
     */
    fun Context.launchAppWithAction(action: String?, categories: List<String?>?) {
        launchAppWithAction(action, categories, null)
    }

    /**
     * 使用Action启动app
     *
     * @param action
     * @param categories
     * @param bundle
     */
    @Suppress("unused")
    fun Context.launchAppWithAction(action: String?, categories: List<String?>?, bundle: Bundle?) {
        val intent = Intent(action)
        bundle?.let { intent.putExtras(it) }
        if (categories.isNullOrEmpty()) throw RuntimeException("categories can not be null or empty")
        for (category in categories) {
            intent.addCategory(category)
        }
        startActivity(intent)
    }

    /**
     * 打开文本文件跳转第三方app
     *
     * @param txtFile 文本文件
     */
    fun Context.openTxtFileWithApp(txtFile: File) {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri: Uri = if (Build.VERSION.SDK_INT >= 24) {
            FileProvider.getUriForFile(this, "$packageName.fileprovider", txtFile)
        } else {
            Uri.fromFile(txtFile)
        }
        intent.setDataAndType(uri, "text/plain")
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(intent)
    }

    /**
     * 安装app
     *
     * @param apkFile 安装apk文件
     *
     * @link android.Manifest.permission#REQUEST_INSTALL_PACKAGES
     */
    @Suppress("DEPRECATION")
    fun Context.installAppWithApkFile(apkFile: File) {
        // 创建安装程序的Intent
        val installIntent = Intent(Intent.ACTION_INSTALL_PACKAGE)
        installIntent.data = if (Build.VERSION.SDK_INT >= 24) {
            FileProvider.getUriForFile(this, "$packageName.fileprovider", apkFile)
        } else {
            Uri.fromFile(apkFile)
        }
        // 授予URI临时权限以进行安装
        installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        // 启动安装程序
        startActivity(installIntent)
    }

    /**
     * 通过浏览器下载apk文件
     *
     * @param apkUrl 下载地址
     */
    fun Context.downloadFileWithBrowser(apkUrl: String) {
        Intent(Intent.ACTION_VIEW).apply {
            this.data = Uri.parse(apkUrl)
            startActivity(this)
        }
    }

}