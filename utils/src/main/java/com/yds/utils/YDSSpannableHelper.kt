package com.yds.utils

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 14:09
 * 页面描述: 文字颜色或背景颜色修改帮助类
 * tips：1.新增span点击事件
 */
@Suppress("unused")
object YDSSpannableHelper {
    private var builder: SpannableStringBuilder? = null

    /**
     * 作者: YDS
     * 时间: 2023/5/29 14:13
     * 描述: 支持多种类型展示 文字颜色+背景颜色
     ***/
    fun setTextSpans(
        mTextView: TextView,
        content: String,
        keywords: List<SpanTextBean>?,
        listener: OnSpanClickListener?
    ) {
        builder = SpannableStringBuilder(content)

        if (!keywords.isNullOrEmpty()) {
            for (spanTextBean in keywords) {
                val spanType = spanTextBean.spanType
                if (spanType == SpanTextBean.SpanType.SPAN_TYPE_FOREGROUND) {
                    addForegroundColorSpanItem(
                        content,
                        spanTextBean.keyword,
                        spanTextBean.color,
                        spanTextBean.isClickable,
                        listener
                    )
                } else if (spanType == SpanTextBean.SpanType.SPAN_TYPE_BACKGROUND) {
                    addBackgroundColorSpanItem(
                        content,
                        spanTextBean.keyword,
                        spanTextBean.color,
                        spanTextBean.isClickable,
                        listener
                    )
                }
            }
        }

        mTextView.movementMethod = LinkMovementMethod.getInstance()
        mTextView.text = builder
    }

    /**
     * 作者: YDS
     * 时间: 2023/5/29 14:13
     * 描述: 单一类型展示 文字颜色/背景颜色
     ***/
    fun setTextSpan(
        mTextView: TextView,
        content: String,
        spanTextBean: SpanTextBean?,
        listener: OnSpanClickListener?
    ) {
        builder = SpannableStringBuilder(content)

        spanTextBean?.let {
            val spanType = it.spanType
            if (spanType == SpanTextBean.SpanType.SPAN_TYPE_FOREGROUND) {
                addForegroundColorSpanItem(
                    content,
                    it.keyword,
                    it.color,
                    it.isClickable,
                    listener
                )
            } else if (spanType == SpanTextBean.SpanType.SPAN_TYPE_BACKGROUND) {
                addBackgroundColorSpanItem(
                    content,
                    it.keyword,
                    it.color,
                    it.isClickable,
                    listener
                )
            }
        }

        mTextView.movementMethod = LinkMovementMethod.getInstance()
        mTextView.text = builder
    }

    /**
     * 作者: YDS
     * 时间: 2023/5/29 14:13
     * 描述: 文字颜色
     ***/
    private fun addForegroundColorSpanItem(
        content: String,
        keyword: String,
        color: Int,
        isClickable: Boolean,
        listener: OnSpanClickListener?
    ) {
        if (content.isNotEmpty()) {
            var start = content.indexOf(keyword)
            while (start != -1) {
                val end = start + keyword.length
                val mColorSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        if (isClickable) {
                            listener?.onSpanClick(keyword)
                        }
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.color = color
                        ds.isUnderlineText = false
                    }
                }
                builder?.setSpan(mColorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                start = content.indexOf(keyword, end)
            }
        }
    }

    /**
     * 作者: YDS
     * 时间: 2023/5/29 14:13
     * 描述: 文字背景颜色
     ***/
    private fun addBackgroundColorSpanItem(
        content: String,
        keyword: String,
        color: Int,
        isClickable: Boolean,
        listener: OnSpanClickListener?
    ) {
        if (content.isNotEmpty()) {
            var start = content.indexOf(keyword)
            while (start != -1) {
                val end = start + keyword.length
                val mColorSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        if (isClickable) {
                            listener?.onSpanClick(keyword)
                        }
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.bgColor = color
                    }
                }
                builder?.setSpan(mColorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                start = content.indexOf(keyword, end)
            }
        }
    }

    /**
     * 作者: YDS
     * 时间: 2023/5/29 14:11
     * 描述: 文字类型实体类
     ***/
    class SpanTextBean(
        var keyword: String,
        var spanType: Int,
        var color: Int,
        var isClickable: Boolean = false
    ) {
        object SpanType {
            /*文字*/
            const val SPAN_TYPE_FOREGROUND = 1
            /*背景*/
            const val SPAN_TYPE_BACKGROUND = 2
        }
    }

    /**
     * 作者: YDS
     * 时间: 2023/5/29 15:23
     * 描述: TextSpan点击事件接口
     ***/
    interface OnSpanClickListener {
        fun onSpanClick(keyword: String)
    }
}