package com.yds.utils

import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.text.format.Formatter

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 11:42
 * 页面描述: 手机基本信息获取-帮助类
 */
@Suppress("unused")
object YDSMobileCheckHelper : YDSBaseHelper() {
    /*** 作者: YDS 时间: 2023/5/29 11:42 描述: 检测手机基本信息 ***/
    fun checkBasicInfo(): String {
        return "**手机信息** \n生产厂商：${Build.MANUFACTURER} \n品牌：${Build.BRAND} \n型号：${Build.MODEL} \n手机硬件版本：${Build.VERSION.RELEASE} \n应用开发版本：${Build.VERSION.SDK_INT} "
    }

    /*** 作者: YDS 时间: 2023/5/29 11:44 描述: 检测手机内存信息 ***/
    fun checkMemoryInfo(): String {
        val maxMemory =
            (Runtime.getRuntime().maxMemory() * 1.0 / (1024 * 1024)).toInt()
        val totalMemory =
            (Runtime.getRuntime().totalMemory() * 1.0 / (1024 * 1024)).toInt()
        val freeMemory =
            (Runtime.getRuntime().freeMemory() * 1.0 / (1024 * 1024)).toInt()
        return "**内存信息** \n最大分配内存：${maxMemory}M \n分配的总内存：${totalMemory}M \n剩余可用内存：${freeMemory}M \n应用已用内存：${totalMemory - freeMemory}M"
    }

    /*** 作者: YDS 时间: 2023/5/29 11:45 描述: 检测储存卡信息 ***/
    @Suppress("DEPRECATION")
    fun checkSDCardInfo(): String {
        // 获得手机内部存储控件的状态
        val dataFileDir = Environment.getDataDirectory()
        // 获得一个磁盘状态对象
        val stat = StatFs(dataFileDir.path)
        val blockSize = stat.blockSize.toLong() // 获得一个扇区的大小
        val totalBlocks = stat.blockCount.toLong() // 获得扇区的总数
        val availableBlocks = stat.availableBlocks.toLong() // 获得可用的扇区数量
        // 总空间
        val totalMemory = Formatter.formatFileSize(getContext(), totalBlocks * blockSize)
        // 可用空间
        val availableMemory = Formatter.formatFileSize(getContext(), availableBlocks * blockSize)
        val dataMemory = "\n总容量: $totalMemory \n可用空间: $availableMemory"
        return "**储存卡信息** $dataMemory"
    }
}