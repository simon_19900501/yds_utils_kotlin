package com.yds.utils

import android.Manifest
import android.app.Activity
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract
import android.text.TextUtils
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import com.permissionx.guolindev.PermissionX
import com.permissionx.guolindev.callback.RequestCallback
import java.lang.Exception
import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 10:43
 * 页面描述: 日历提醒帮助类
 * tips: Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR
 */
@Suppress("unused")
object YDSCalendarReminderHelper : YDSBaseHelper() {
    private const val calenderUrl = "content://com.android.calendar/calendars"
    private const val calenderEventUrl = "content://com.android.calendar/events"
    private const val calenderReminderUrl = "content://com.android.calendar/reminders"
    private const val calendarsName = "boohee"
    private const val calendarsAccountName = "BOOHEE@boohee.com"
    private const val calendarsAccountType = "com.android.boohee"
    private const val calendarsDisplayName = "BOOHEE账户"

    /**
     * 添加日历事件
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    fun addCalendarEvent(
        context: Activity?,
        title: String?,
        description: String?,
        reminderTime: Long,
        previousDate: Int
    ) {
        PermissionX.init(context as FragmentActivity?)
            .permissions(
                Manifest.permission.WRITE_CALENDAR,
                Manifest.permission.READ_CALENDAR
            )
            .onExplainRequestReason { scope, deniedList, _ ->
                scope.showRequestReasonDialog(
                    deniedList,
                    "即将申请的权限是程序必须依赖的权限",
                    "我已明白"
                )
            }
            .onForwardToSettings { scope, deniedList ->
                scope.showForwardToSettingsDialog(
                    deniedList,
                    "您需要去应用程序设置当中手动开启权限",
                    "我已明白"
                )
            }
            .request(object : RequestCallback {
                override fun onResult(
                    allGranted: Boolean,
                    grantedList: List<String?>?,
                    deniedList: List<String?>
                ) {
                    if (allGranted) {
                        val calId = checkAndAddCalendarAccount(getContext()) // 获取日历账户的id
                        if (calId < 0) { // 获取账户id失败直接返回，添加日历事件失败
                            return
                        }

                        // 添加日历事件
                        val mCalendar = Calendar.getInstance()
                        mCalendar.timeInMillis = reminderTime // 设置开始时间
                        val start = mCalendar.time.time
                        mCalendar.timeInMillis = start + 1 * 60 * 1000 // 设置终止时间，开始时间加10分钟
                        val end = mCalendar.time.time
                        val event = ContentValues()
                        event.put("title", title)
                        event.put("description", description)
                        event.put("calendar_id", calId) // 插入账户的id
                        event.put(CalendarContract.Events.DTSTART, start)
                        event.put(CalendarContract.Events.DTEND, end)
                        event.put(CalendarContract.Events.HAS_ALARM, 1) // 设置有闹钟提醒
                        event.put(
                            CalendarContract.Events.EVENT_TIMEZONE,
                            "Asia/Shanghai"
                        ) // 这个是时区，必须有
                        val newEvent = getContext().contentResolver.insert(
                            Uri.parse(calenderEventUrl),
                            event
                        ) ?: return // 添加日历事件失败直接返回

                        // 事件提醒的设定
                        val values = ContentValues()
                        values.put(
                            CalendarContract.Reminders.EVENT_ID,
                            ContentUris.parseId(newEvent)
                        )
                        values.put(
                            CalendarContract.Reminders.MINUTES,
                            previousDate * 24 * 60
                        ) // 提前previousDate天有提醒
                        values.put(
                            CalendarContract.Reminders.METHOD,
                            CalendarContract.Reminders.METHOD_ALERT
                        )
                        getContext().contentResolver.insert(
                            Uri.parse(calenderReminderUrl),
                            values
                        ) ?: return //添加事件提醒失败直接返回
                    } else {
                        Toast.makeText(getContext(), "您拒绝了如下权限：$deniedList", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            })
    }

    /**
     * 删除日历事件
     */
    fun deleteCalendarEvent(context: Context?, title: String) {
        if (context == null) {
            return
        }
        val eventCursor =
            context.contentResolver.query(Uri.parse(calenderEventUrl), null, null, null, null)
        try {
            if (eventCursor == null) { //查询返回空值
                return
            }
            if (eventCursor.count > 0) {
                //遍历所有事件，找到title跟需要查询的title一样的项
                eventCursor.moveToFirst()
                while (!eventCursor.isAfterLast) {
                    val eventTitle = eventCursor.getString(eventCursor.getColumnIndex("title"))
                    if (!TextUtils.isEmpty(title) && title == eventTitle) {
                        val id =
                            eventCursor.getInt(eventCursor.getColumnIndex(CalendarContract.Calendars._ID)) //取得id
                        val deleteUri =
                            ContentUris.withAppendedId(Uri.parse(calenderEventUrl), id.toLong())
                        val rows = context.contentResolver.delete(deleteUri, null, null)
                        if (rows == -1) { //事件删除失败
                            return
                        }
                    }
                    eventCursor.moveToNext()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            eventCursor?.close()
        }
    }

    /**
     * 检查是否已经添加了日历账户，如果没有添加先添加一个日历账户再查询
     * 获取账户成功返回账户id，否则返回-1
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun checkAndAddCalendarAccount(context: Context): Int {
        val oldId = checkCalendarAccount(context)
        return if (oldId >= 0) {
            oldId
        } else {
            val addId = addCalendarAccount(context)
            if (addId >= 0) {
                checkCalendarAccount(context)
            } else {
                -1
            }
        }
    }

    /**
     * 检查是否存在现有账户，存在则返回账户id，否则返回-1
     */
    private fun checkCalendarAccount(context: Context): Int {
        val userCursor =
            context.contentResolver.query(Uri.parse(calenderUrl), null, null, null, null)
        try {
            // 查询返回空值
            if (userCursor == null) {
                return -1
            }
            val count = userCursor.count
            // 存在现有账户，取第一个账户的id返回
            return if (count > 0) {
                userCursor.moveToFirst()
                userCursor.getInt(userCursor.getColumnIndex(CalendarContract.Calendars._ID))
            } else {
                -1
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            userCursor?.close()
        }

        return -1
    }

    /**
     * 添加日历账户，账户创建成功则返回账户id，否则返回-1
     */
    private fun addCalendarAccount(context: Context): Long {
        val timeZone = TimeZone.getDefault()
        val value = ContentValues()
        value.put(CalendarContract.Calendars.NAME, calendarsName)
        value.put(CalendarContract.Calendars.ACCOUNT_NAME, calendarsAccountName)
        value.put(CalendarContract.Calendars.ACCOUNT_TYPE, calendarsAccountType)
        value.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, calendarsDisplayName)
        value.put(CalendarContract.Calendars.VISIBLE, 1)
        value.put(CalendarContract.Calendars.CALENDAR_COLOR, Color.BLUE)
        value.put(
            CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,
            CalendarContract.Calendars.CAL_ACCESS_OWNER
        )
        value.put(CalendarContract.Calendars.SYNC_EVENTS, 1)
        value.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, timeZone.id)
        value.put(CalendarContract.Calendars.OWNER_ACCOUNT, calendarsAccountName)
        value.put(CalendarContract.Calendars.CAN_ORGANIZER_RESPOND, 0)
        var calendarUri = Uri.parse(calenderUrl)
        calendarUri = calendarUri.buildUpon()
            .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, calendarsAccountName)
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, calendarsAccountType)
            .build()
        val result = context.contentResolver.insert(calendarUri, value)
        return if (result == null) -1 else ContentUris.parseId(result)
    }
}