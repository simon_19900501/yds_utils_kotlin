package com.yds.utils

import android.annotation.SuppressLint
import com.yds.utils.YDSToastHelper.showShortToastBottom
import java.text.SimpleDateFormat
import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 17:10
 * 页面描述: 日期时间-帮助类
 */
@Suppress("unused")
@SuppressLint("SimpleDateFormat")
object YDSDateTimeHelper : YDSBaseHelper() {
    /* 日期格式 */
    private val sdf_ymdhms = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val sdf_ymdhm = SimpleDateFormat("yyyy-MM-dd HH:mm")
    private val sdf_ymd = SimpleDateFormat("yyyy-MM-dd")
    private val sdf_hms = SimpleDateFormat("HH:mm:ss")
    private val sdf_hm = SimpleDateFormat("HH:mm")

    /*** 作者: YDS 时间: 2023/5/29 17:11 描述: 获取：年-月-日 时:分:秒 ***/
    val getYmdhms: String = sdf_ymdhms.format(System.currentTimeMillis())

    /*** 作者: YDS 时间: 2023/5/29 17:14 描述: 时间戳转换日期：年-月-日 时:分:秒 ***/
    fun formatTimeStampToYmdhms(timeStamp: Long): String {
        return sdf_ymdhms.format(timeStamp)
    }

    /*** 作者: YDS 时间: 2023/5/29 17:18 描述: 日期转换时间戳 ***/
    fun formatYmdhmsToTimeStamp(dateTime: String): Long {
        return try {
            sdf_ymdhms.parse(dateTime)!!.time
        } catch (e: Exception) {
            e.printStackTrace()
            return -1
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 17:20 描述: 获取：年-月-日 时:分 ***/
    val getYmdhm: String = sdf_ymdhm.format(System.currentTimeMillis())

    /*** 作者: YDS 时间: 2023/5/29 17:25 描述: 时间戳转换日期：年-月-日 时:分 ***/
    fun formatTimeStampToYmdhm(timeStamp: Long): String {
        return sdf_ymdhm.format(timeStamp)
    }

    /*** 作者: YDS 时间: 2023/5/29 17:26 描述: 日期转换时间戳 ***/
    fun formatYmdhmToTimeStamp(dateTime: String): Long {
        return try {
            sdf_ymdhm.parse(dateTime)!!.time
        } catch (e: Exception) {
            e.printStackTrace()
            return -1
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 17:20 描述: 获取：年-月-日 ***/
    val getYmd: String = sdf_ymd.format(System.currentTimeMillis())

    /*** 作者: YDS 时间: 2023/5/29 17:29 描述: 时间戳转换日期：年-月-日 ***/
    fun formatTimeStampToYmd(timeStamp: Long): String {
        return sdf_ymd.format(timeStamp)
    }

    /*** 作者: YDS 时间: 2023/5/29 17:29 描述: 日期转换时间戳 ***/
    fun formatYmdToTimeStamp(dateTime: String): Long {
        return try {
            sdf_ymd.parse(dateTime)!!.time
        } catch (e: Exception) {
            e.printStackTrace()
            return -1
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 17:31 描述: 获取：时:分:秒 ***/
    val getHms: String = sdf_hms.format(System.currentTimeMillis())

    /*** 作者: YDS 时间: 2023/5/29 17:31 描述: 获取：时:分 ***/
    val getHm: String = sdf_hm.format(System.currentTimeMillis())

    /*** 作者: YDS 时间: 2023/5/29 17:42 描述: 获取剩余时间 ***/
    fun getRemainTime(startDateTime: String, endDateTime: String): List<Long> {
        val mListTemp: MutableList<Long> = ArrayList()
        // 一秒钟的毫秒数
        val secondMsec: Long = 1000
        // 一天的毫秒数
        val dayMsec = 24 * 60 * 60 * secondMsec
        // 一小时的毫秒数
        val hourMsec = 60 * 60 * secondMsec
        // 一分钟的毫秒数
        val minuteMsec = 60 * secondMsec
        //毫秒差
        val diffMsec: Long

        if (startDateTime.isNotEmpty() && startDateTime.contains(" ")
            && endDateTime.isNotEmpty() && endDateTime.contains(" ")
        ) {
            try {
                // 获得两个时间的毫秒时间差异
                diffMsec =
                    sdf_ymdhms.parse(endDateTime)!!.time - sdf_ymdhms.parse(startDateTime)!!.time
                if (diffMsec >= 0) {
                    /* 判断结束时间是否大于开始时间 */
                    // 计算差多少天
                    val diffDay = diffMsec / dayMsec
                    mListTemp.add(diffDay)
                    // 计算差多少小时
                    val diffHour = diffMsec % dayMsec / hourMsec
                    mListTemp.add(diffHour)
                    // 计算差多少分钟
                    val diffMin = diffMsec % dayMsec % hourMsec / minuteMsec
                    mListTemp.add(diffMin)
                    // 计算差多少秒//输出结果
                    val diffSec =
                        diffMsec % dayMsec % dayMsec % minuteMsec / secondMsec
                    mListTemp.add(diffSec)
                } else {
                    getContext().showShortToastBottom("The start time cannot be greater than the end time")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            getContext().showShortToastBottom("Date Time format error")
        }

        return mListTemp
    }

    /**
     * 比对大小
     *
     * @param startDateTime 年-月-日 时:分:秒
     * @param endDateTime   年-月-日 时:分:秒
     */
    fun isBiggerThan(startDateTime: String, endDateTime: String): Boolean {
        return formatYmdhmsToTimeStamp(startDateTime) > formatYmdhmsToTimeStamp(endDateTime)
    }

    /**
     * 当前日期是星期几
     *
     * @param dateTime 年-月-日 时:分:秒 or 年-月-日
     */
    fun getWeekOfDate(dateTime: String): String {
        val weeks = arrayOf("日", "一", "二", "三", "四", "五", "六")
        if (dateTime.isEmpty()) {
            throw RuntimeException("dateTime is empty")
        }
        val isIntegrity = dateTime.contains(" ")
        val date = Date(
            if (isIntegrity) formatYmdhmsToTimeStamp(dateTime) else formatYmdToTimeStamp(dateTime)
        )
        val calendar = Calendar.getInstance()
        calendar.time = date
        var week = calendar[Calendar.DAY_OF_WEEK] - 1
        if (week < 0) week = 0
        return weeks[week]
    }

}