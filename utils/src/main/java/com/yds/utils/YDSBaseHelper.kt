package com.yds.utils

import android.app.Application
import android.content.Context
/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 9:50
 * 页面描述: 工具类的基类初始化
 */
@Suppress("unused")
open class YDSBaseHelper {
    companion object {
        private var mApplication: Application? = null

        /*** 作者: YDS 时间: 2023/5/29 9:50 描述: 初始化Application ***/
        fun init(application: Application) {
            if (null == mApplication) {
                mApplication = application
            }
        }

        /*** 作者: YDS 时间: 2023/5/29 9:50 描述: 获取上下文对象 ***/
        fun getContext(): Context {
            if (null == mApplication) {
                throw RuntimeException("INIT YDSBaseHelper FIRST")
            }
            return mApplication!!.applicationContext
        }
    }

}