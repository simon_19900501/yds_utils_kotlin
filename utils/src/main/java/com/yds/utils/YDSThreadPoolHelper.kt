package com.yds.utils

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.util.concurrent.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 14:20
 * 页面描述: 线程池-帮助类
 * 1、增加生命周期观察
 */
@Suppress("unused")
object YDSThreadPoolHelper : LifecycleObserver {
    private val mHandler: Handler?
    private var executorServiceSingle: ExecutorService? = null
    private var executorServiceFixed: ExecutorService? = null
    private var executorServiceCached: ExecutorService? = null
    private var executorServiceScheduled: ScheduledExecutorService? = null

    init {
        mHandler = Handler(Looper.getMainLooper())
    }

    /*** 作者: YDS 时间: 2023/5/29 14:22 描述: 单线程池 可用于串行 ***/
    fun initSingleThreadPool(runnable: Runnable, listener: ThreadRunDoneListener?) {
        if (null == executorServiceSingle) {
            executorServiceSingle = Executors.newSingleThreadExecutor()
        }
        val future = executorServiceSingle!!.submit(runnable)
        isThreadRunDone(future, listener)
    }

   /*** 作者: YDS 时间: 2023/5/29 14:22 描述: 固定线程池-6 ***/
    fun initFixedThreadPool(runnable: Runnable, listener: ThreadRunDoneListener?) {
        if (null == executorServiceFixed) {
            executorServiceFixed = Executors.newFixedThreadPool(6)
        }
        val future = executorServiceFixed!!.submit(runnable)
        isThreadRunDone(future, listener)
    }

    /*** 作者: YDS 时间: 2023/5/29 14:22 描述: 缓存线程池 ***/
    fun initCachedThreadPool(runnable: Runnable, listener: ThreadRunDoneListener?) {
        if (null == executorServiceCached) {
            executorServiceCached = Executors.newCachedThreadPool()
        }
        val future = executorServiceCached!!.submit(runnable)
        isThreadRunDone(future, listener)
    }

    /*** 作者: YDS 时间: 2023/5/29 14:22 描述: 定时+周期任务线程池 ***/
    fun initScheduledThreadPoolPeriod(
        runnable: Runnable,
        delay: Int,
        period: Int
    ): ScheduledExecutorService? {
        if (null == executorServiceScheduled) {
            executorServiceScheduled = Executors.newScheduledThreadPool(3)
        }
        executorServiceScheduled!!.scheduleAtFixedRate(
            runnable,
            delay.toLong(),
            period.toLong(),
            TimeUnit.MILLISECONDS
        )
        return executorServiceScheduled
    }

    /*** 作者: YDS 时间: 2023/5/29 14:22 描述: 线程是否运行完成 ***/
    private fun isThreadRunDone(future: Future<*>, mThreadRunDoneListener: ThreadRunDoneListener?) {
        val done = future.isDone
        mThreadRunDoneListener?.threadRunResult(done)
        //如果未完成100毫秒检测一次
        if (!done) {
            mHandler!!.postDelayed({ isThreadRunDone(future, mThreadRunDoneListener) }, 100)
        }
    }

   /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 线程池结果回调 ***/
    interface ThreadRunDoneListener {
        fun threadRunResult(isDone: Boolean)
    }

    /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 销毁全部线程池 ***/
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroyThreads() {
        mHandler?.removeCallbacksAndMessages(null)
        destroySingleThread()
        destroyFixedThread()
        destroyCachedThread()
        destroyScheduledThread()
    }

    /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 关闭单线程池 ***/
    @Suppress("unused")
    fun destroySingleThread() {
        if (null != executorServiceSingle) {
            executorServiceSingle!!.shutdownNow()
            executorServiceSingle = null
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 关闭固定线程池 ***/
    @Suppress("unused")
    fun destroyFixedThread() {
        if (null != executorServiceFixed) {
            executorServiceFixed!!.shutdownNow()
            executorServiceFixed = null
        }
    }

   /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 关闭缓存线程池 ***/
    @Suppress("unused")
    fun destroyCachedThread() {
        if (null != executorServiceCached) {
            executorServiceCached!!.shutdownNow()
            executorServiceCached = null
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 14:21 描述: 关闭定时线程池 ***/
    @Suppress("unused")
    fun destroyScheduledThread() {
        if (null != executorServiceScheduled) {
            executorServiceScheduled!!.shutdownNow()
            executorServiceScheduled = null
        }
    }
}