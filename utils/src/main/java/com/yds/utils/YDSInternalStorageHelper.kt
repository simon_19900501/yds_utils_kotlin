package com.yds.utils

import android.content.Context

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 11:52
 * 页面描述: 内部储存帮助类
 */
@Suppress("unused")
object YDSInternalStorageHelper{

    /**
     * 1.内部存储
     * 2./data/user/0/com.xxx.xxx/files 或 /data/data/com.xxx.xxx/files
     * 3.不需要额外的权限来读取或在返回的路径下写入文件
     * 4.当应用被卸载时，文件数据被清除
     * 5.一般情况下，非root手机不能访问
     */
    fun Context.getAppFilesDir(): String? = applicationContext.filesDir.path

    /**
     * 1.内部存储
     * 2./data/user/0/com.xxx.xxx/cache 或 /data/data/com.xxx.xxx/cache
     * 3.不需要额外的权限来读取或在返回的路径下写入文件
     * 4.当应用被卸载时，文件数据被清除
     * 5.当该文件夹超过当前被分配的最大缓存时，系统将自动删除该目录中的文件为其他地方提供需要空间，当未超出时则不会
     * 6.一般情况下，非root手机不能访问
     */
    fun Context.getAppCacheDir(): String? = applicationContext.cacheDir.path

    /**
     * 1.内部存储
     * 2./data/user/0/com.xxx.xxx/code_cache 或 /data/data/com.xxx.xxx/code_cache
     * 3.不需要额外的权限来读取或在返回的路径下写入文件
     * 4.当应用被卸载时，文件数据被清除
     * 5.一般情况下，非root手机不能访问
     * 6.Android 5.0新增接口，低于5.0手机不支持
     */
    fun Context.getAppCodeCacheDir(): String? = applicationContext.codeCacheDir.path


}