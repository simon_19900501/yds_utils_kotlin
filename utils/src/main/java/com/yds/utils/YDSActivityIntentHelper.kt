package com.yds.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 9:12
 * 页面描述: startActivity跳转帮助类
 */
@Suppress("unused")
class YDSActivityIntentHelper {

    companion object {
        /*** 作者: YDS 时间: 2023/5/29 8:55 描述: 普通跳转 ***/
        inline fun <reified T : Activity> Context.startActivityTo() {
            startActivity(Intent(this, T::class.java))
        }

        /*** 作者: YDS 时间: 2023/5/29 9:09 描述: 携带参数跳转 ***/
        inline fun <reified T : Activity> Context.startActivityWithBundleTo(bundle: Bundle? = null) {
            Intent(this, T::class.java).apply {
                bundle?.let {
                    putExtras(it)
                }
                startActivity(this)
            }
        }

        /*** 作者: YDS 时间: 2023/5/29 9:12 描述: 开启新栈跳转 ***/
        inline fun <reified T : Activity> Context.startActivityWithNewTask(bundle: Bundle? = null) {
            YDSActivityStackHelper.finishOthersActivity(T::class.java)
            Intent(this, T::class.java).apply {
                bundle?.let {
                    putExtras(it)
                }
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(this)
            }
        }
    }

}