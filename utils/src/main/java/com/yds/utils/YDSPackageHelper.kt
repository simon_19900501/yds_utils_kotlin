package com.yds.utils

import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import com.yds.utils.entity.AppInfoBean
import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 11:10
 * 页面描述: 包名相关帮助类
 * tips：<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
 */
@Suppress("unused")
object YDSPackageHelper : YDSBaseHelper() {

    private var packageInfo: PackageInfo = getContext().packageManager
        .getPackageInfo(getContext().packageName, 0)

    /*** 作者: YDS 时间: 2023/5/29 11:11 描述: 获取本地版本号 ***/
    @Suppress("DEPRECATION")
    fun getLocalVersionCode(): Long {
        val versionCode =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                packageInfo.longVersionCode
            } else {
                packageInfo.versionCode.toLong()
            }
        return versionCode
    }

    /*** 作者: YDS 时间: 2023/5/29 11:11 描述: 获取本地版本名 ***/
    fun getLocalVersionName(): String = packageInfo.versionName

    /*** 作者: YDS 时间: 2023/5/29 11:23 描述: 将版本名转换成版本号 ***/
    fun convertToVersionCode(versionName: String): Int =
        Integer.parseInt(versionName.replace(".", ""))

    /**
     * 查询所有应用信息
     * @param isSystemApplication 是否是系统应用
     */
    @Suppress("DEPRECATION")
    fun queryAllAppPackages(isSystemApplication: Boolean): List<AppInfoBean> {
        val mListSystemAppInfos: MutableList<AppInfoBean> = ArrayList<AppInfoBean>()
        val mListThirdAppInfos: MutableList<AppInfoBean> = ArrayList<AppInfoBean>()
        val packageManager = getContext().packageManager
        // 获取到所有安装了的应用程序的信息，包括那些卸载了的，但没有清除数据的应用程序
        val installedApplications =
            packageManager.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES)
        for (installedApplication in installedApplications) {
            val appInfo = AppInfoBean()
            appInfo.name = installedApplication.loadLabel(packageManager).toString()
            appInfo.icon = installedApplication.loadIcon(packageManager)
            appInfo.packageName = installedApplication.packageName
            if (installedApplication.flags and ApplicationInfo.FLAG_SYSTEM <= 0) {
                // 三方应用
                mListThirdAppInfos.add(appInfo)
            } else {
                // 系统应用
                mListSystemAppInfos.add(appInfo)
            }
        }
        return if (isSystemApplication) mListSystemAppInfos else mListThirdAppInfos
    }

    /**
     * 判断该包名的应用是否安装
     * @param packageName 应用包名
     */
    @Suppress("unused")
    fun checkAppExist(packageName: String): Boolean {
        try {
            val packageManager = getContext().packageManager
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                packageManager.getApplicationInfo(
                    packageName,
                    PackageManager.MATCH_UNINSTALLED_PACKAGES
                )
            } else {
                packageManager.getApplicationInfo(packageName, 0)
            }
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return false
    }

    /**
     * 判断该包名的应用是否安装
     * @param packageName 应用包名
     */
    fun uninstallApp(packageName: String): Boolean {
        if (checkAppExist(packageName)) {
            val packageURI = Uri.parse("package:$packageName")
            val intent = Intent(Intent.ACTION_DELETE)
            intent.data = packageURI
            getContext().startActivity(intent)
            return true
        }
        return false
    }

}