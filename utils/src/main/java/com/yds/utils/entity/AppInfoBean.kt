package com.yds.utils.entity

import android.graphics.drawable.Drawable
import java.io.Serializable

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 11:24
 * 页面描述: app信息实体类
 */
class AppInfoBean : Serializable {
    companion object {
        /** 序列号  */
        const val serialVersionUID = -6660233212727684115L
    }

    /** 名称  */
    var name: String? = null

    /** 图标  */
    var icon: Drawable? = null

    /** 包名  */
    var packageName: String? = null

    override fun toString(): String {
        return "AppInfoBean{" +
                "name='" + name + '\'' +
                ", icon=" + icon +
                ", packageName='" + packageName + '\'' +
                '}'
    }
}