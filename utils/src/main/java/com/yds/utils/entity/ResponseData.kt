package com.yds.utils.entity

/**
 * 作者:     YDS
 * 创建时间: 2023/3/24 14:43
 * 页面描述: 接收数据的基类
 */
data class ResponseData<out T>(
    val code: Int,
    val message: String,
    val result: T
)
