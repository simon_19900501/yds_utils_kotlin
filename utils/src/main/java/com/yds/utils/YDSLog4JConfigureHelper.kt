package com.yds.utils

import android.annotation.SuppressLint
import android.content.Context
import de.mindpipe.android.logging.log4j.LogConfigurator
import java.text.SimpleDateFormat
import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/4/28 13:20
 * 页面描述: log4j日志设置
 */
@Suppress("unused")
object YDSLog4JConfigureHelper {
    private const val DATA_PATTERN = "yyyy-MM-dd"

    @SuppressLint("SimpleDateFormat")
    fun configure(context: Context) {
        val logConfigurator = LogConfigurator()

        //日志文件路径地址
        val df = SimpleDateFormat(DATA_PATTERN)
        val name = "/log_" + df.format(Date()) + ".txt"
        val fileName = context.filesDir.toString() + name
        //设置文件名
        logConfigurator.fileName = fileName
        //设置root日志输出级别 默认为DEBUG
        // logConfigurator.setRootLevel(Level.DEBUG);
        // 设置日志输出级别
        // logConfigurator.setLevel("org.apache", Level.INFO);
        //设置 输出到日志文件的文字格式 默认 %d %-5p [%c{2}]-[%L] %m%n
        logConfigurator.filePattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} %-5p %-40.40c{1} [%L]- %m%n"
        //设置输出到控制台的文字格式 默认%m%n
        logConfigurator.logCatPattern = "%C{1} [%L] %m"
        //设置总文件大小
        logConfigurator.maxFileSize = (1024 * 1024 * 20).toLong()
        //设置最大备份数量
        logConfigurator.maxBackupSize = 10
        //设置所有消息是否被立刻输出 默认为true,false 不输出
        logConfigurator.isImmediateFlush = true
        //是否本地控制台打印输出 默认为true ，false不输出
        logConfigurator.isUseLogCatAppender = true
        //设置是否启用文件附加,默认为true。false为覆盖文件
        logConfigurator.isUseFileAppender = true
        //设置是否重置配置文件，默认为true
        logConfigurator.isResetConfiguration = true
        //是否显示内部初始化日志,默认为false
        logConfigurator.isInternalDebugging = false
        logConfigurator.configure()
    }
}