package com.yds.utils

import android.content.Context
import android.view.Gravity
import android.widget.Toast

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 10:50
 * 页面描述: 吐司帮助类
 */
@Suppress("unused")
object YDSToastHelper {

    /*** 作者: YDS 时间: 2023/5/29 10:56 描述: 吐司--短--中间 ***/
    fun Context.showShortToastCenter(msg: String?): Toast? {
        if (msg.isNullOrEmpty()) return null
        return Toast.makeText(this, msg, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.CENTER, 0, 0)
            show()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 10:56 描述: 吐司--短--底部 ***/
    fun Context.showShortToastBottom(msg: String?): Toast? {
        if (msg.isNullOrEmpty()) return null
        return Toast.makeText(this, msg, Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.BOTTOM, 0, 150)
            show()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 10:56 描述: 吐司--长--中间 ***/
    fun Context.showLongToastCenter(msg: String?): Toast? {
        if (msg.isNullOrEmpty()) return null
        return Toast.makeText(this, msg, Toast.LENGTH_LONG).apply {
            setGravity(Gravity.CENTER, 0, 0)
            show()
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 10:56 描述: 吐司--长--底部 ***/
    fun Context.showLongToastBottom(msg: String?): Toast? {
        if (msg.isNullOrEmpty()) return null
        return Toast.makeText(this, msg, Toast.LENGTH_LONG).apply {
            setGravity(Gravity.BOTTOM, 0, 150)
            show()
        }
    }
}