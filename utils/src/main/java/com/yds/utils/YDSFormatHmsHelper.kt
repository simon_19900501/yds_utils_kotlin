package com.yds.utils
/**
 * 作者:     YDS
 * 创建时间: 2024/8/28 10:14
 * 页面描述: 格式化时间
 */
object YDSFormatHmsHelper {


    /*** 作者: YDS 时间: 2023/5/30 8:54 描述: 格式化样式 00:00:00 小时:分钟:秒 ***/
    fun formatHMS(num: Int): String {
        if (num <= 0) return "00:00"
        // 格式化-秒
        val secondTemp: String
        val second = num % 60
        secondTemp = if (second < 10) {
            "0$second"
        } else {
            "$second"
        }

        // 格式化-分
        val minuteTemp: String
        val minute = num / 60 % 60
        minuteTemp = if (minute < 10) {
            "0$minute"
        } else {
            "$minute"
        }

        // 格式化-时
        val hourTemp: String
        val hour = num / 60 / 60
        hourTemp = if (hour < 10) {
            "0$hour"
        } else {
            "$hour"
        }
        return "$hourTemp:$minuteTemp:$secondTemp"
    }

    /*** 作者: YDS 时间: 2023/5/30 8:58 描述: 格式化样式 00:00 分钟:秒 ***/
    fun formatMS(num: Int): String {
        if (num <= 0) return "00:00"

        // 格式化-秒
        val secondTemp: String
        val second = num % 60
        secondTemp = if (second < 10) {
            "0$second"
        } else {
            "$second"
        }

        // 格式化-分
        val minuteTemp: String
        val minute = num / 60
        minuteTemp = if (minute < 10) {
            "0$minute"
        } else {
            "$minute"
        }
        return "$minuteTemp:$secondTemp"
    }

}