package com.yds.utils

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 13:34
 * 页面描述: 软键盘展示与隐藏-帮助类
 */
@Suppress("unused")
object YDSSoftKeyBoardHelper {
    /**
     * 展示软键盘
     * @param view    :一般为EditText
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun showKeyboard(view: View) {
        val inputMethodManager = view.context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(view, 0)
    }

    /**
     * 隐藏软键盘
     * @param view    :一般为EditText
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun hideKeyboard(view: View) {
        val inputMethodManager = view.context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     * 显示软键盘
     *
     * @param activity 当前Activity
     */
    fun showSoftInput(activity: Activity) {
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    /**
     * 隐藏软键盘
     *
     * @param activity 当前Activity
     */
    fun hideSoftInput(activity: Activity) {
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
    }

}