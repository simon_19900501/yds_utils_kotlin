package com.yds.utils

import android.text.TextUtils
import android.util.Log

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 13:21
 * 页面描述: 日志-帮助类
 * tips：Application初始化 YDSLogHelper.init("Simon",true);
 */
@Suppress("unused")
object YDSLogHelper {
    private var LOG_FLAG = "Simon" // Log标识

    private var debug = true // 是否开启bug调试标识

    /**
     * @param logFlag Log标识(全局共用，也可在具体调用时单独设置)
     * @param debugSetting  是否开启bug调试
     */
    fun init(logFlag: String, debugSetting: Boolean) {
        if (!TextUtils.isEmpty(logFlag)) {
            LOG_FLAG = logFlag
        }
        debug = debugSetting
    }

    /**
     * log.i
     *
     * @param targetMsg 目标信息
     */
    fun i(targetMsg: String) {
        // 判断是否含有 lambda 表达式
        var lambda = false

        // 获取打印所在行数和方法
        val s = Thread.currentThread().stackTrace
        for (value in s) {
            if (value.methodName.startsWith("lambda")) {
                lambda = true
            }
        }
        val methodName: String
        val className: String
        if (!lambda) {
            methodName = s[3].methodName
            className = s[3].toString()
        } else {
            methodName = s[5].methodName
            className = s[5].toString()
        }
        if (debug) {
            Log.i(
                LOG_FLAG,
                "\n目标类名：${className.substring(className.indexOf("("))} \n目标标识：$LOG_FLAG \n目标方法：$methodName \n目标信息：$targetMsg"
            )
        }
    }

    /**
     * log.i
     *
     * @param logKey    目标标识
     * @param targetMsg 目标信息
     */
    fun i(logKey: String, targetMsg: String) {
        // 判断是否含有 lambda 表达式
        var lambda = false

        // 获取打印所在行数和方法
        val s = Thread.currentThread().stackTrace
        for (value in s) {
            if (value.methodName.startsWith("lambda")) {
                lambda = true
            }
        }
        val methodName: String
        val className: String
        if (!lambda) {
            methodName = s[3].methodName
            className = s[3].toString()
        } else {
            methodName = s[5].methodName
            className = s[5].toString()
        }
        if (debug) {
            Log.i(
                logKey,
                "\n目标类名：${className.substring(className.indexOf("("))} \n目标标识：$logKey \n目标方法：$methodName \n目标信息：$targetMsg"
            )
        }
    }

    /**
     * log.e
     * @param targetMsg 目标信息
     */
    fun e(targetMsg: String) {
        // 判断是否含有 lambda 表达式
        var lambda = false

        // 获取打印所在行数和方法
        val s = Thread.currentThread().stackTrace
        for (value in s) {
            if (value.methodName.startsWith("lambda")) {
                lambda = true
            }
        }
        val methodName: String
        val className: String
        if (!lambda) {
            methodName = s[3].methodName
            className = s[3].toString()
        } else {
            methodName = s[5].methodName
            className = s[5].toString()
        }
        if (debug) {
            Log.e(
                LOG_FLAG,
                "\n目标类名：${className.substring(className.indexOf("("))} \n目标标识：$LOG_FLAG \n目标方法：$methodName \n目标信息：$targetMsg"
            )
        }
    }

    /**
     * log.e
     * @param logKey    目标标识
     * @param targetMsg 目标信息
     */
    fun e(logKey: String, targetMsg: String) {
        // 判断是否含有 lambda 表达式
        var lambda = false

        // 获取打印所在行数和方法
        val s = Thread.currentThread().stackTrace
        for (value in s) {
            if (value.methodName.startsWith("lambda")) {
                lambda = true
            }
        }
        val methodName: String
        val className: String
        if (!lambda) {
            methodName = s[3].methodName
            className = s[3].toString()
        } else {
            methodName = s[5].methodName
            className = s[5].toString()
        }
        if (debug) {
            Log.e(
                logKey,
                "\n目标类名：${className.substring(className.indexOf("("))} \n目标标识：$logKey \n目标方法：$methodName \n目标信息：$targetMsg"
            )
        }
    }

    /*** 作者: YDS 时间: 2023/5/29 13:15 描述: 格式化json串 ***/
    private fun formatString(text: String): String {
        val json = StringBuilder()
        var indentString = ""
        for (letter in text) {
            when (letter) {
                '{', '[' -> {
                    json.append(
                        """
                        $indentString$letter
                        """.trimIndent()
                    )
                    indentString += "\t"
                    json.append(indentString)
                }
                '}', ']' -> {
                    indentString = indentString.replaceFirst("\t".toRegex(), "")
                    json.append(
                        """
                        $indentString$letter
                        """.trimIndent()
                    )
                }
                ',' -> json.append(
                    """
                    $letter
                    $indentString
                    """.trimIndent()
                )
                else -> json.append(letter)
            }
        }
        return json.toString()
    }

    /*** 作者: YDS 时间: 2023/5/29 13:15 描述: unicode转中文 ***/
    fun decode(unicodeStr: String?): String? {
        if (unicodeStr == null) {
            return null
        }
        val retBuf = StringBuffer()
        val maxLoop = unicodeStr.length
        var i = 0
        while (i < maxLoop) {
            if (unicodeStr[i] == '\\') {
                if (i < maxLoop - 5 && (unicodeStr[i + 1] == 'u' || unicodeStr[i + 1] == 'U')) try {
                    retBuf.append(unicodeStr.substring(i + 2, i + 6).toInt(16).toChar())
                    i += 5
                } catch (localNumberFormatException: NumberFormatException) {
                    retBuf.append(unicodeStr[i])
                } else retBuf.append(unicodeStr[i])
            } else {
                retBuf.append(unicodeStr[i])
            }
            i++
        }
        return retBuf.toString()
    }
}