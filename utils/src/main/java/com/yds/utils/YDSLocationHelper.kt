package com.yds.utils

import android.Manifest
import android.content.Context
import android.location.LocationManager
import android.location.LocationListener
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.os.Bundle
import android.util.Log

/**
 * 作者:     YDS
 * 创建时间: 2023/6/5 14:15
 * 页面描述: 获取定位（GPS+基站）
 * tips: Manifest.permission.ACCESS_FINE_LOCATION
 *       Manifest.permission.ACCESS_COARSE_LOCATION
 *       Manifest.permission.ACCESS_WIFI_STATE
 */
@Suppress("unused")
class YDSLocationHelper(
    private val mContext: Context,
    private val minTime: Long = 1000,
    private val minDistance: Float = 1f
) {
    private val TAG = javaClass.name

    private val mLocationManager: LocationManager?
    private val mLocationListener: LocationListener?

    private var mOnLocationListener: OnLocationListener? = null

    init {
        mLocationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mLocationListener = MyLocationListener()
    }

    fun start(listener: OnLocationListener) {
        mOnLocationListener = listener
        location
    }

    fun stop() {
        if (mLocationManager != null && mLocationListener != null) {
            mLocationManager.removeUpdates(mLocationListener)
        }
    }

    private val location: Unit
        get() {
            if (ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_WIFI_STATE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Log.e(TAG, "getLocation: no permission")
                return
            }
            val allProviders = mLocationManager!!.allProviders
            for (allProvider in allProviders) {
                Log.e(TAG, "allProvider = $allProvider")
            }
            val gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val networkEnabled =
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            Log.e(TAG, "gpsEnabled = $gpsEnabled networkEnabled = $networkEnabled")
            if (gpsEnabled) {
                mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    minTime,
                    minDistance,
                    mLocationListener!!,
                    Looper.getMainLooper()
                )
            }
            if (networkEnabled) {
                mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    minTime,
                    minDistance,
                    mLocationListener!!,
                    Looper.getMainLooper()
                )
            }
        }

    private fun handleLocation(location: Location?) {
        if (location == null) {
            Log.e(TAG, "handleLocation: null location")
            return
        }
        val latitude = location.latitude
        val longitude = location.longitude
        if (mOnLocationListener != null) {
            mOnLocationListener!!.onLocation(latitude, longitude)
        }
    }

    interface OnLocationListener {
        fun onLocation(latitude: Double, longitude: Double)
    }

    private inner class MyLocationListener : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.e(TAG, "onLocationChanged: $location")
            handleLocation(location)
        }

        override fun onProviderEnabled(provider: String) {
            Log.e(TAG, "onProviderEnabled: $provider")
            location
        }

        override fun onProviderDisabled(provider: String) {
            Log.e(TAG, "onProviderDisabled: provider=$provider")
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.d(TAG, "onStatusChanged: provider=$provider, status=$status")
        }
    }

}