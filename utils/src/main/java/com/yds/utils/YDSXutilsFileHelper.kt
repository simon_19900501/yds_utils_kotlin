package com.yds.utils

import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.text.TextUtils
import com.google.gson.Gson
import org.xutils.common.Callback
import org.xutils.common.util.KeyValue
import org.xutils.http.RequestParams
import org.xutils.http.body.MultipartBody
import org.xutils.x
import java.io.File
import java.io.FileNotFoundException
import java.util.*

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 16:12
 * 页面描述: Xutils文件上传与下载-帮助类
 */
@Suppress("unused")
object YDSXutilsFileHelper : YDSBaseHelper() {
    private var cancelable: Callback.Cancelable? = null
    private val mCallbackMap: MutableMap<String, Callback.Cancelable?> =
        HashMap<String, Callback.Cancelable?>()

    // 受保护储存路径
    var protectVisitPath: String = getContext().filesDir.path.toString() + File.separator

    /**
     * 上传文件
     *
     * @param fileUrl   网络地址
     * @param filePaths 本地地址
     * @param obj       其它参数
     * @param callBack  请求回调
     */
    fun uploadFile(
        fileUrl: String,
        filePaths: List<String>,
        obj: Any?,
        callBack: CommonFileCallBack<String?>
    ) {
        val list: MutableList<KeyValue> = ArrayList<KeyValue>()

        // 构建RequestParams对象，传入请求的服务器地址URL
        val params = RequestParams(fileUrl)
        params.connectTimeout = 300 * 1000
        params.readTimeout = 300 * 1000
        params.isAsJsonContent = true

        // 其它参数
        if (null != obj) {
            val json: String = Gson().toJson(obj)
            if (!TextUtils.isEmpty(json)) {
                list.add(KeyValue("parameters", json))
            }
        }

        // 本地文件地址列表
        if (!filePaths.isNullOrEmpty()) {
            for (filePath in filePaths) {
                list.add(KeyValue("file", File(filePath)))
            }
        }
        val body = MultipartBody(list, "UTF-8")
        params.requestBody = body
        cancelable = x.http().post(params, object : Callback.ProgressCallback<String> {
            override fun onWaiting() {
                callBack.onWaiting()
            }

            override fun onStarted() {
                callBack.onStarted()
            }

            override fun onLoading(total: Long, current: Long, isDownloading: Boolean) {
                callBack.onLoading((current * 100 / total).toInt())
            }

            override fun onSuccess(result: String) {
                callBack.onSuccess(result)
            }

            override fun onError(ex: Throwable, isOnCallback: Boolean) {
                callBack.onError(ex.message)
            }

            override fun onCancelled(cex: Callback.CancelledException?) {
                callBack.onCancelled()
            }

            override fun onFinished() {
                callBack.onFinished()
            }
        })
        mCallbackMap[fileUrl] = cancelable
    }

    /**
     * 下载文件
     *
     * @param fileUrl
     * @param filePath
     * @param callBack
     */
    fun downloadFile(fileUrl: String, filePath: String, callBack: CommonFileCallBack<String>) {
        downloadFile(fileUrl, filePath, false, callBack)
    }

    /**
     * 下载文件
     *
     * @param fileUrl
     * @param filePath
     * @param updateAlbum
     * @param callBack
     */
    @Suppress("unused")
    fun downloadFile(
        fileUrl: String, filePath: String,
        updateAlbum: Boolean, callBack: CommonFileCallBack<String>
    ) {
        if (filePath.isEmpty()) {
            throw RuntimeException("FILE PATH IS EMPTY")
        }
        val requestParams = RequestParams(parseUtf8Url(fileUrl))
        requestParams.saveFilePath = filePath
        cancelable = x.http().get(requestParams, object : Callback.ProgressCallback<File> {
            override fun onWaiting() {
                callBack.onWaiting()
            }

            override fun onStarted() {
                callBack.onStarted()
            }

            override fun onLoading(total: Long, current: Long, isDownloading: Boolean) {
                callBack.onLoading((current * 100 / total).toInt())
            }

            override fun onSuccess(file: File) {
                callBack.onSuccess(file.path)
                if (updateAlbum) {
                    updateAlbum(file.path)
                }
            }

            override fun onError(ex: Throwable, isOnCallback: Boolean) {
                callBack.onError(ex.message.toString())
            }

            override fun onCancelled(cex: Callback.CancelledException?) {
                callBack.onCancelled()
            }

            override fun onFinished() {
                callBack.onFinished()
            }
        })
        mCallbackMap[fileUrl] = cancelable
    }

    /*** 作者: YDS 时间: 2023/5/29 16:23 描述: 将url进行encode，解决部分手机无法下载含有中文url的文件的问题 ***/
    @Suppress("DEPRECATION")
    fun parseUtf8Url(url: String): String {
        val sb = StringBuffer()
        for (element in url) {
            if (element.toInt() in 0..255) {
                sb.append(element)
            } else {
                val b: ByteArray = try {
                    element.toString().toByteArray(charset("utf-8"))
                } catch (ex: Exception) {
                    println(ex)
                    ByteArray(0)
                }
                for (j in b.indices) {
                    var k = b[j].toInt()
                    if (k < 0) k += 256
                    sb.append("%" + Integer.toHexString(k).uppercase(Locale.getDefault()))
                }
            }
        }
        return sb.toString()
    }

    /*** 作者: YDS 时间: 2023/5/29 16:25 描述: 利用文件url转换出文件名 ***/
    @Suppress("unused")
    fun parseFileName(url: String): String {
        var fileName: String? = null
        try {
            fileName = url.substring(url.lastIndexOf("/") + 1)
        } finally {
            fileName = fileName ?: System.currentTimeMillis().toString()
        }
        return fileName
    }

    /*** 作者: YDS 时间: 2023/5/29 16:27 描述: 更新相册 ***/
    @Suppress("DEPRECATION")
    fun updateAlbum(filePath: String) {
        val file = File(filePath)
        val fileName = parseFileName(filePath)
        try {
            MediaStore.Images.Media.insertImage(
                getContext().contentResolver,
                file.absolutePath,
                fileName,
                null
            )
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        getContext().sendBroadcast(
            Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                Uri.parse(file.absolutePath)
            )
        )
    }

    /*** 作者: YDS 时间: 2023/5/29 16:30 描述: 请求回调 ***/
    interface CommonFileCallBack<T> {
        fun onWaiting() {}
        fun onStarted() {}
        fun onSuccess(result: T)
        fun onLoading(progress: Int) {}
        fun onError(errorMsg: T) {}
        fun onCancelled() {}
        fun onFinished() {}
    }

    /*** 作者: YDS 时间: 2023/5/29 16:30 描述: 查询Callback.Cancelable ***/
    fun queryRequestCancelable(): Map<String, Callback.Cancelable?> {
        return mCallbackMap
    }

    /*** 作者: YDS 时间: 2023/5/29 16:30 描述: 取消请求-通过url ***/
    fun requestCancelByUrl(url: String) {
        val cancelable: Callback.Cancelable? = mCallbackMap[url]
        cancelable?.cancel()
    }

    /*** 作者: YDS 时间: 2023/5/29 16:30 描述: 取消请求-全部 ***/
    fun requestCancel() {
        if (mCallbackMap.isEmpty()) return
        val iterator: Iterator<Map.Entry<String, Callback.Cancelable?>> =
            mCallbackMap.entries.iterator()
        while (iterator.hasNext()) {
            val entry: Map.Entry<String, Callback.Cancelable?> = iterator.next()
            val cancelable: Callback.Cancelable? = entry.value
            cancelable?.cancel()
        }
    }
}