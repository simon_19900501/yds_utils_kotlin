package com.yds.utils.mvvm

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.text.TextUtils

import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import org.xutils.image.ImageOptions
import org.xutils.x

@Suppress("unused")
object BindingAdapters {

    @BindingAdapter(
        value = ["imageUrl", "placeholderImage", "errorImage", "isCircular", "radius"],
        requireAll = false
    )
    @JvmStatic
    fun loadImageUrl(
        imageView: AppCompatImageView,
        imageUrl: String?,
        placeholderImage: Drawable?,
        errorImage: Drawable?,
        isCircular: Boolean = false,
        radius: Int = 0
    ) {
        val options = ImageOptions.Builder()
            .setLoadingDrawable(placeholderImage)               // 设置占位图
            .setFailureDrawable(errorImage)                     // 设置错误图
            .setIgnoreGif(false)                                // 是否忽略Gif图片
            .setImageScaleType(ImageView.ScaleType.CENTER_CROP) // 设置缩放模式
            .setCircular(isCircular)
            .setRadius(radius)
            .build()
        x.image().bind(imageView, imageUrl, options)
    }

    @BindingAdapter(value = ["imageBitmap"])
    @JvmStatic
    fun loadImageBitmap(imageView: AppCompatImageView, imageBitmap: Bitmap?) {
        imageView.setImageBitmap(imageBitmap)
    }

    @BindingAdapter(value = ["text", "default"], requireAll = false)
    @JvmStatic
    fun setText(textView: AppCompatTextView, text: String?, default: String?) {
        textView.text = if (TextUtils.isEmpty(text)) default else text
    }

    @BindingAdapter(value = ["drawableStart"])
    @JvmStatic
    fun setStartDrawable(textView: AppCompatTextView, drawableStart: Drawable?) {
        if (null == drawableStart) {
            textView.setCompoundDrawables(null, null, null, null)
        } else {
            drawableStart.setBounds(0, 0, drawableStart.minimumWidth, drawableStart.minimumHeight)
            textView.setCompoundDrawables(drawableStart, null, null, null)
        }
    }

    @BindingAdapter(value = ["drawableEnd"])
    @JvmStatic
    fun setEndDrawable(textView: AppCompatTextView, drawableEnd: Drawable?) {
        if (null == drawableEnd) {
            textView.setCompoundDrawables(null, null, null, null)
        } else {
            drawableEnd.setBounds(0, 0, drawableEnd.minimumWidth, drawableEnd.minimumHeight)
            textView.setCompoundDrawables(drawableEnd, null, null, null)
        }
    }

    @BindingAdapter(value = ["visibility"])
    @JvmStatic
    fun setViewInvisible(view: View, visibility: Boolean) {
        view.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
    }

    @BindingAdapter(value = ["visibility"])
    @JvmStatic
    fun setViewGone(view: View, visibility: Boolean) {
        view.visibility = if (visibility) View.VISIBLE else View.GONE
    }

    private var startShortTime: Long = 0

    @BindingAdapter(value = ["onClickDelayShort"], requireAll = false)
    @JvmStatic
    fun setOnClickDelayShort(view: View, listener: View.OnClickListener?) {
        view.setOnClickListener {
            val currentTimeMillis = System.currentTimeMillis()
            if (currentTimeMillis - startShortTime >= 200) {
                startShortTime = currentTimeMillis
                listener?.onClick(view)
            }
        }
    }

    private var startLongTime: Long = 0

    @BindingAdapter(value = ["onClickDelayLong"], requireAll = false)
    @JvmStatic
    fun setOnClickDelayLong(view: View, listener: View.OnClickListener?) {
        view.setOnClickListener {
            val currentTimeMillis = System.currentTimeMillis()
            if (currentTimeMillis - startLongTime >= 1000) {
                startLongTime = currentTimeMillis
                listener?.onClick(view)
            }
        }
    }

    @BindingAdapter(value = ["layout_width"])
    @JvmStatic
    fun setViewWidth(view: View, layout_width: Float) {
        val width = view.layoutParams.width
        if (width != layout_width.toInt()) {
            view.apply {
                this.layoutParams = layoutParams.apply {
                    this.width = layout_width.toInt()
                }
            }
        }
    }

    @BindingAdapter(value = ["layout_height"])
    @JvmStatic
    fun setViewHeight(view: View, layoutHeight: Float) {
        val height = view.layoutParams.height
        if (height != layoutHeight.toInt()) {
            view.apply {
                this.layoutParams = layoutParams.apply {
                    this.height = layoutHeight.toInt()
                }
            }
        }
    }


}