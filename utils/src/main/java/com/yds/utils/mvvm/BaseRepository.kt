package com.yds.utils.mvvm

import com.yds.utils.entity.ResponseData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * 作者:     YDS
 * 创建时间: 2023/5/30 9:44
 * 页面描述: 基类Repository
 */
@Suppress("unused")
open class BaseRepository {

    suspend fun <T : Any> request(call: suspend () -> ResponseData<T>): ResponseData<T> {
        return withContext(Dispatchers.IO) {
            call.invoke()
        }.apply {
          when (code) {
                200 -> this
                else -> throw RuntimeException(code.toString())
            }
        }
    }

}