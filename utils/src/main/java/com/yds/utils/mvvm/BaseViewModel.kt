package com.yds.utils.mvvm


import android.view.View
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import org.apache.log4j.Logger
/**
 * 作者:     YDS
 * 创建时间: 2023/5/30 9:42
 * 页面描述: 基类ViewModel
 */
@Suppress("unused")
open class BaseViewModel : ViewModel(), LifecycleObserver {
    @Suppress("unused")
    val logger: Logger = Logger.getLogger(javaClass.name)

    private val _clickEvent = MutableLiveData<View>()
    val clickEvent: LiveData<View> = _clickEvent

    fun onViewClicked(view: View) {
        _clickEvent.value = view
    }

    fun launchUI( block: suspend CoroutineScope.() -> Unit) = viewModelScope.launch(Dispatchers.Main) {
        try {
            withTimeout(30000) {
                block()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            logger.error("launchUI Exception -> ${e.message}")
        }
    }

}