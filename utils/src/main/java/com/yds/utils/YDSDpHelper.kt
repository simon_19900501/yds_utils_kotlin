package com.yds.utils

/**
 * 作者:     YDS
 * 创建时间: 2023/5/29 9:52
 * 页面描述: dp sp 帮助类
 */
@Suppress("unused")
object YDSDpHelper : YDSBaseHelper() {
    /*** 作者: YDS 时间: 2023/5/29 14:34 描述: 根据手机的分辨率从 dp 的单位 转成为 px(像素) ***/
    fun dpToPx(dpValue: Float): Int {
        val scale: Float = getContext().resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    /*** 作者: YDS 时间: 2023/5/29 14:33 描述: 根据手机的分辨率从 px(像素) 的单位 转成为 dp ***/
    fun pxToDp(pxValue: Float): Int {
        val scale: Float = getContext().resources.displayMetrics.density
        return (pxValue / scale + 0.5f).toInt()
    }

    /**
     * 获取屏幕尺寸
     *
     * @return sizes[0]-宽度 sizes[1]-高度
     */
    fun getScreenSize(): IntArray {
        val sizes = IntArray(2)
        getContext().resources.displayMetrics.apply {
            sizes[0] = this.widthPixels
            sizes[1] = this.heightPixels
        }
        return sizes
    }
}