package com.yds.utils_kotlin

import android.app.Application
import com.yds.utils.YDSBaseHelper
import com.yds.utils.YDSLogHelper

class BaseApplication :Application() {
    companion object {
        lateinit var instance: BaseApplication
            private set
    }


    override fun onCreate() {
        super.onCreate()

        instance = this

        YDSBaseHelper.init(this)
        YDSLogHelper.init("Simon",true)
    }
}