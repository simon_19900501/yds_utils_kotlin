package com.yds.utils_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.yds.utils.*
import com.yds.utils.YDSToastHelper.showShortToastBottom

class MainActivity : AppCompatActivity() {
    var aaa: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showShortToastBottom("hello")

        YDSCountDownHelper.run(totalSeconds = 10, onStart = {

        }, onSecond = {

        }, onComplete = {

        })

        val formatHMS = YDSFormatHmsHelper.formatHMS(5000)
        Log.i("YDSimon", "formatHMS = $formatHMS")

    }
}